cmake_minimum_required(VERSION 3.26)
project(uebung01)

include(CheckLibraryExists)

CHECK_LIBRARY_EXISTS(m sin "" HAVE_LIB_M)

if (HAVE_LIB_M)
    set(EXTRA_LIBS ${EXTRA_LIBS} m)
endif (HAVE_LIB_M)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(main
        src/main.c
        src/prime.c
        src/prime.h
        src/ggT.c
        src/ggT.h
        src/kgV.c
        src/kgV.h
)

target_link_libraries(main PUBLIC ${EXTRA_LIBS})

# Fetch GoogleTest
Include(FetchContent)
FetchContent_Declare(
        googletest
        GIT_REPOSITORY https://github.com/google/googletest.git
        GIT_TAG        v1.15.2
)
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)

# Build Tests
enable_testing()

add_executable(test_prime
        src/prime.c
        src/prime.h
        src/prime.test.cpp
)

add_executable(test_ggT_kgV
        src/ggT.c
        src/ggT.h
        src/kgV.c
        src/kgV.h
        src/ggT.test.cpp
        src/kgV.test.cpp
)

add_executable(test_ggT_rec
        src/ggT.c
        src/ggT.h
        src/ggt_rec.test.cpp
)

target_link_libraries(test_prime PRIVATE GTest::gtest_main)
target_link_libraries(test_prime PUBLIC ${EXTRA_LIBS})
target_link_libraries(test_ggT_kgV PRIVATE GTest::gtest_main)
target_link_libraries(test_ggT_kgV PUBLIC ${EXTRA_LIBS})
target_link_libraries(test_ggT_rec PRIVATE GTest::gtest_main)
target_link_libraries(test_ggT_rec PUBLIC ${EXTRA_LIBS})
target_include_directories(test_prime PRIVATE src/)
target_include_directories(test_ggT_kgV PRIVATE src/)
include(GoogleTest)
gtest_discover_tests(test_prime)
gtest_discover_tests(test_ggT_kgV)
gtest_discover_tests(test_ggT_rec)