#include <gtest/gtest.h>
extern "C" {
#include "ggT.h"
}
TEST(ggT_rec, zero) {
    EXPECT_EQ(ggT_rec(0, 0), 0);
    EXPECT_EQ(ggT_rec(1, 0), 1);
    EXPECT_EQ(ggT_rec(0, 1), 1);
}

TEST(ggT_rec, negative) {
    EXPECT_EQ(ggT_rec(-4, -2), ggT_rec(4, 2));
    EXPECT_EQ(ggT_rec(4, -2), ggT_rec(4, 2));
    EXPECT_EQ(ggT_rec(-4, 2), ggT_rec(4, 2));
}

TEST(ggT_rec, same) { EXPECT_EQ(ggT_rec(8, 8), 8); }

TEST(ggT_rec, positive) {
    EXPECT_EQ(ggT_rec(3528, 3780), 252);
    EXPECT_EQ(ggT_rec(70, 42), 14);
    EXPECT_EQ(ggT_rec(768, 912), 48);
}
