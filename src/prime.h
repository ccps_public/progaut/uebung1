#ifndef PRIME_CATCH2_PRIME_H
#define PRIME_CATCH2_PRIME_H

#include <stdbool.h>

bool isPrime(int primeCandidate);

#endif  // PRIME_CATCH2_PRIME_H
