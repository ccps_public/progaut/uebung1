#include <gtest/gtest.h>
extern "C" {
#include "ggT.h"
}
TEST(ggT, zero) {
    EXPECT_EQ(ggT(0, 0), 0);
    EXPECT_EQ(ggT(1, 0), 1);
    EXPECT_EQ(ggT(0, 1), 1);
}

TEST(ggT, negative) {
    EXPECT_EQ(ggT(-4, -2), ggT(4, 2));
    EXPECT_EQ(ggT(4, -2), ggT(4, 2));
    EXPECT_EQ(ggT(-4, 2), ggT(4, 2));
}

TEST(ggT, equal) { EXPECT_EQ(ggT(8, 8), 8); }

TEST(ggT, positive) {
    EXPECT_EQ(ggT(3528, 3780), 252);
    EXPECT_EQ(ggT(70, 42), 14);
    EXPECT_EQ(ggT(768, 912), 48);
}
