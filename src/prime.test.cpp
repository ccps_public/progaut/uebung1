#include <gtest/gtest.h>

extern "C" {
#include "prime.h"
}

TEST(Primes, Negativ) { EXPECT_FALSE(isPrime(-2)); }

TEST(Primes, Zero) { EXPECT_FALSE(isPrime(0)); }

TEST(Primes, One) { EXPECT_FALSE(isPrime(1)); }

TEST(Prime, Small) {
    EXPECT_TRUE(isPrime(2));
    EXPECT_TRUE(isPrime(3));
    EXPECT_FALSE(isPrime(4));
    EXPECT_TRUE(isPrime(5));
    EXPECT_FALSE(isPrime(6));
    EXPECT_TRUE(isPrime(7));
    EXPECT_FALSE(isPrime(8));
    EXPECT_FALSE(isPrime(9));
}

TEST(Primes, Large) {
    EXPECT_TRUE(isPrime(241));
    EXPECT_TRUE(isPrime(9857));
    EXPECT_TRUE(isPrime(99971));
    EXPECT_FALSE(isPrime(9841));
}
