#include <gtest/gtest.h>
extern "C" {
#include "kgV.h"
}
TEST(kgv, positiv) {
    EXPECT_EQ(kgV(12, 18), 36);
    EXPECT_EQ(kgV(18, 24), 72);
    EXPECT_EQ(kgV(1, 24), 24);
    EXPECT_EQ(kgV(18, 1), 18);
    EXPECT_EQ(kgV(18, 18), 18);
}

TEST(kgv, negative) {
    EXPECT_EQ(kgV(-18, 24), 72);
    EXPECT_EQ(kgV(18, -24), 72);
    EXPECT_EQ(kgV(-18, -24), 72);
}

TEST(kgv, zero) {
    EXPECT_EQ(kgV(0, 24), 0);
    EXPECT_EQ(kgV(18, 0), 0);
    EXPECT_EQ(kgV(0, -24), 0);
    EXPECT_EQ(kgV(0, 0), 0);
}
